package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.support.xml.SqlXmlFeatureNotImplementedException;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class threadControllerTests {
    private Thread thread;
    private Vote vote;
    private List<Post> posts;

    @BeforeEach
    @DisplayName("post creation test")
    void createPostTest() {
        thread = new Thread(
            "kamtim",
            new Timestamp(1612388978445L),
            "forum",
            "message",
            "slug",
            "title",
            100
        );

        posts = Collections.<Post>emptyList();
    }

    @Test
    void checkIdOrSlugStringTest() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenReturn(thread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(
                    thread,
                    controller.CheckIdOrSlug("slug"),
                    "Result for succeeding slug check"
                );
            }
        }
    }


    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatesForum() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenReturn(thread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(
                    ResponseEntity.status(HttpStatus.CREATED).body(posts),
                    controller.createPost("slug", posts),
                    "Result for succeeding post creation"
                );
            }

            assertEquals(thread, ThreadDAO.getThreadBySlug("slug"));
        }
    }

    @Test
    @DisplayName("Not found error for post creation test")
    void notFoundErrorForForumCreation() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenThrow(new SqlXmlFeatureNotImplementedException("DataAccessException"));

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                ResponseEntity responseEntity = controller.createPost("slug", posts);

                assertEquals(
                    HttpStatus.NOT_FOUND,
                    responseEntity.getStatusCode(),
                    "Result for not found error post creation"
                );

                assertEquals(
                    "Раздел не найден.",
                    ((Message) responseEntity.getBody()).getData(),
                    "Result for not found error post creation"
                );
            }
        }
    }

    @Test
    @DisplayName("Not found error for post creation test")
    void conflictErrorForForumCreation() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenReturn(thread);

            threadMock.when(() -> ThreadDAO.createPosts(thread, posts, Collections.<User>emptyList()))
                .thenThrow(new SqlXmlFeatureNotImplementedException("DataAccessException"));

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {

                threadController controller = new threadController();

                ResponseEntity responseEntity = controller.createPost("slug", posts);

                assertEquals(
                    HttpStatus.CONFLICT,
                    responseEntity.getStatusCode(),
                    "Result for not found error post creation"
                );

                assertEquals(
                    "Хотя бы один родительский пост отсутсвует в текущей ветке обсуждения.",
                    ((Message) responseEntity.getBody()).getData(),
                    "Result for not found error post creation"
                );
            }
        }
    }

    @Test
    @DisplayName("Correct test for Posts")
    void testForPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenReturn(thread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertThrows(
                    NullPointerException.class,
                    () -> controller.Posts("no-such-slug", 5, 0, "sort", true)
                );
            }
            assertEquals(thread, ThreadDAO.getThreadBySlug("slug"));
        }
    }

    @Test
    @DisplayName("Correct test for change")
    void testForChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenReturn(thread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertThrows(
                    NullPointerException.class,
                    () -> controller.change("no-such-slug", thread)
                );
            }
        }
    }

    @Test
    @DisplayName("Correct test for info")
    void testForInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenReturn(thread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(null),
                    controller.info("no-such-slug"),
                    "Trying to get by not existing slug"
                );
            }
        }
    }

    @Test
    @DisplayName("Correct test for createVote")
    void testForCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug"))
                .thenReturn(thread);

            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                assertThrows(
                    NullPointerException.class,
                    () -> controller.createVote("slug", vote)
                );
            }
        }
    }
}
